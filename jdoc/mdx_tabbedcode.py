from markdown import Extension
from markdown.preprocessors import Preprocessor
import re
from pygments import highlight
from pygments.lexers import get_lexer_by_name, guess_lexer
from pygments.formatters import HtmlFormatter
from pygments.formatters.html import _escape_html_table
from pygments.token import Token

RE = re.compile(r'^`{3}[ ]*(?P<label>[^\n]*)\n?(?P<code>.*?)\n`{3}[ ]*\n?', re.MULTILINE|re.DOTALL)

GROUP_WRAP = '<div class="tc">{}<pre>{}</pre></div>'
CODE_WRAP = '<pre><code>{}</code></pre>'
TAB_WRAP = '<label><input type="radio" name="{group}"{checked}><div>{name}{code}</div></label>'

class CodeHtmlFormatter(HtmlFormatter):

    def _format_lines(self, tokensource):
        lsep = self.lineseparator
        # for <span style=""> lookup only
        getcls = self.ttype2class.get
        c2s = self.class2style
        escape_table = _escape_html_table

        lspan = ''
        line = ''
        for ttype, value in tokensource:
            if ttype in (Token.Name, Token.Text, Token.Punctuation):
                cspan = ''
            else:
                cls = self._get_css_class(ttype)
                if cls == 'm' or cls == 'mi' or cls == 'mh' or cls == 'mf' or cls == 'bp':
                    # We highlight string and number literals the same
                    cls = 's'

                elif cls == 'nc':
                    # Ignore Name.Class
                    cls = ''

                elif cls == 'nl':
                    # Ignore
                    cls = ''

                elif cls == 'nf':
                    cls = ''

                elif cls == 'nb':
                    cls = 'kt'

                elif cls == 'kd' or cls == 'kc' or cls == 'kr' or cls == 'ow' or cls == 'kn':
                    cls = 'k'

                elif cls == 's1' or cls == 's2' or cls == 'se':
                    cls = 's'

                elif cls == "c1" or cls == 'cpf':
                    cls = 'c'

                if cls and cls not in ('kt', 'k', 's', 'c', 'o', 'w', 'na', 'cp', 'nn'):
                    CodeHtmlFormatter.warn("Ignoring highlight for class '{}' - we should blacklist it or add a highlight rule: \"{}\"".format(cls, value))
                    cls = ''
                cspan = cls and '<span class="%s">' % cls or ''

            parts = value.translate(escape_table).split('\n')

            # for all but the last line
            for part in parts[:-1]:
                if line:
                    if lspan != cspan:
                        line += (lspan and '</span>') + cspan + part + \
                                (cspan and '</span>') + lsep
                    else: # both are the same
                        line += part + (lspan and '</span>') + lsep
                    yield 1, line
                    line = ''
                elif part:
                    yield 1, cspan + part + (cspan and '</span>') + lsep
                else:
                    yield 1, lsep
            # for the last line
            if line and parts[-1]:
                if lspan != cspan:
                    line += (lspan and '</span>') + cspan + parts[-1]
                    lspan = cspan
                else:
                    line += parts[-1]
            elif parts[-1]:
                line = cspan + parts[-1]
                lspan = cspan
            # else we neither have to open a new span nor set lspan

        if line:
            yield 1, line + (lspan and '</span>') + lsep

    def wrap(self, source, outfile):
        return self._wrap_code(source)

    def _wrap_code(self, source):
        for i, t in source:
            yield i, t


class TabbedCodeExt(Extension):
    def extendMarkdown(self, md, md_globals):
        md.registerExtension(self)
        md.preprocessors.add('tabbedcode',
                                 TabbedCodePreprocessor(md),
                                 ">normalize_whitespace")
class TabbedCodePreprocessor(Preprocessor):
    def run(self, lines):
        text = "\n".join(lines)
        blocks = []
        while 1:
            m = RE.search(text)
            if m:
                try:
                    label = m.group("label")
                    if label.endswith('98') or label.endswith('11'):
                        label = label[:-2]
                    lexer = get_lexer_by_name(label)
                except ValueError:
                    try:
                        lexer = guess_lexer(m.group("code"))
                    except ValueError:
                        lexer = get_lexer_by_name('text')
                formatter = CodeHtmlFormatter()
                code = highlight(m.group("code"), lexer, formatter)
                TabbedCodeExt.found_highlight = True
                if m.start() > 0:
                    blocks.append(text[:m.start()])
                blocks.append((m.group('label'), code))
                text = text[m.end():]
            else:
                blocks.append(text)
                break

        l = []
        last_b = None
        for b in blocks:
            if not l or type(b) is str or type(last_b) is str:
                l.append([b])
            else:
                l[-1].append(b)
            last_b = b

        res = []
        group_name = 0
        for g in l:
            if len(g) == 1:
                if type(g[0]) is str:
                    res.extend(g[0].split("\n"))
                else:
                    res.append(self.markdown.htmlStash.store(CODE_WRAP.format(g[0][1]), safe=True))
            else:
                s = ""
                checked = " checked"
                max_lines = 0
                for (name,code) in g:
                    max_lines = max(max_lines, len(code.split("\n")))
                    code = CODE_WRAP.format(code)
                    s += TAB_WRAP.format(group=group_name, name=name.capitalize(), code=code, checked=checked)
                    TabbedCodeExt.found_tabbed = True
                    checked = ""
                max_lines = "\n"*max_lines
                s = GROUP_WRAP.format(s, max_lines)
                res.append(self.markdown.htmlStash.store(s, safe=True))
                group_name += 1

        return res

    def _escape(self, txt):
        txt = txt.replace('&', '&amp;')
        txt = txt.replace('<', '&lt;')
        txt = txt.replace('>', '&gt;')
        txt = txt.replace('"', '&quot;')
        return txt

def makeExtension(configs=None):
    return TabbedCodeExt()

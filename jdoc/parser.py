import os, json, lesscpy, re, shutil, htmlmin, time, markdown
from os import path
from collections import OrderedDict
from jsmin import jsmin
from PIL import Image
from jinja2 import contextfilter, contextfunction
import subprocess
from subprocess import Popen
from copy import copy
from pygments import highlight
from pygments.lexers import get_lexer_by_name, guess_lexer
from pygments.formatters import HtmlFormatter
from pygments.formatters.html import _escape_html_table
from pygments.token import Token
import xml.etree.ElementTree as ET
import sys

from jdoc.mdx_thumb import ThumbExtension
from jdoc.mdx_highlight import HighlightExtension
from jdoc.mdx_tabbedcode import CodeHtmlFormatter, TabbedCodeExt
from jdoc.mdx_apiref import APIRefExtension

sitemap_urls = set()

TYPEDEF_MAP = {}
TYPEDEF_NAME_REVERSE = {}
T_MAP = {'fn':'functions', 'cls':'classes', 'enum':'enums', 'ns':'namespaces', "def": "defines", "ev":"enum_values", 'f': 'files'}
BYREF = {}
class E:
    def __init__(self, t, name, parent, el, **kw):
        self.static = False
        if el:
            refid = el.attrib['id']
            BYREF[refid] = self
            self.refid = refid
            self.el = el
            self.brief = self.el.find("briefdescription")
            self.detailed = self.el.find("detaileddescription")
            if 'static' in el.attrib:
                if el.attrib['static'] == 'yes':
                    self.static = True
            ini = self.el.find("initializer")
            if ini is not None:
                self.initializer = ''.join(ini.itertext())
            argsstring = self.el.find("argsstring")
            if argsstring is not None:
                self.argsstring = ''.join(argsstring.itertext())

        self.this = self
        self.t = t

        self.name = name.rsplit("::", 1)[-1]
        self.id = "{}-{}".format(self.t, self.name.replace(" ", "_"))
        self.all_children = []
        self.defines = []
        self.namespaces = []
        self.classes = []
        self.enums = []
        self.enum_values = []
        self.functions = []
        self.files = []
        for k,v in kw.items():
            if hasattr(self, k):
                continue
            setattr(self, k, v)


        guess = True
        if guess and t == "fn" and parent and (parent.t == "f" or parent.t == "ns"):
            for param in self.el.findall("param"):
                found = None
                if found is None:
                    found = guess_association(self.el, parent)
                if found is None:
                    found = guess_association(param, parent)
                if found is not None:
                    parent = found
                break


        if parent:
            for c in getattr(parent, T_MAP[t]):
                if c.id == self.id:
                    warn("Duplicate id "+self.id)
                    return
            assert(t in T_MAP)
            self.lang = parent.lang
            getattr(parent, T_MAP[t]).append(self)
            parent.all_children.append(self)
        if parent and parent.name:
            self.sig = parent.sig+"::"+name
        else:
            self.sig = name
        self.parent = parent

    def build_json(self):
        if self.t == "root":
            self.json = OrderedDict([('ns',[]), ("cls",[]), ("fn",[]), ('enum',[])])
        else:
            r = self.root
            ctx = {'this':self, 'functions':self.functions, 'enums':self.enums}
            if self.t in r.json:
                r.json[self.t].append([self.name, self.rootless_url(),
                    docstr(ctx, self.brief, self.id, True, True)])
        for c in self.all_children:
            c.build_json()

    @contextfunction
    def py_parameters(self, ctx):
        for el in self.el.findall("param"):
            decl = el.find('defname').text
            defval = ""
            e = el.find('defval')
            if e is not None:
                defval = e.text
            yield {
                "declaration_name": decl,
                "defval": defval,
            }
    @contextfunction
    def parameters(self, ctx):
        for el in self.el.findall("param"):
            if el.find('declname') is None:
                continue
            decl = el.find('declname').text
            defval = ""
            e = el.find('defval')
            if e is not None:
                defval = e.text
            yield {
                "type": parse_type_to_html(el, ctx),
                "declaration_name": decl,
                "defval": defval,
            }

    @contextfunction
    def type(self, ctx):
        return parse_type_to_html(self.el, ctx)
            

    @property
    def root(self):
        if self.t == "root":
            return self
        return self.parent.root

    def rootless_url(self):
        """ Url without language root """
        if self.parent is None:
            return ""
        if self.t == 'ns' or self.t == 'f':
            return "{}{}/".format(self.parent.rootless_url(), self.name)
        elif self.t == 'fn' or self.t == "def" or self.t == "ev":
            return "{}#{}".format(self.parent.rootless_url(), self.id)
        else:
            return "{}{}/".format(self.parent.rootless_url(), self.id)

    @property
    def url(self):
        if self.parent is None:
            return "/{}/".format(self.lang.id)
        if self.t == 'ns' or self.t == 'f':
            name = self.name
            if name.endswith('.h'):
                name = name[:-2]
            #TODO Collapse root when there is only a single child.
            #if self.parent.t == "root":
                #return self.parent.url
            #else:
            return "{}{}/".format(self.parent.url, name)
        elif self.t == 'fn' or self.t == "def" or self.t == "ev":
            return "{}#{}".format(self.parent.url, self.id)
        else:
            return "{}{}/".format(self.parent.url, self.id)

    @property
    def namespace(self):
        if self.t == 'root':
            return self
        if not self.parent:
            assert(self.t == 'ns')
            return self
        if self.parent.t == 'ns':
            return self.parent
        elif self.parent.t == 'root':
            c = self.sig.rsplit("::")[0]
            for ns in self.parent.namespaces:
                if ns.name == c:
                    return ns
            for ns in self.parent.files:
                if ns.name == c:
                    return ns
            assert(False)
        else:
            return self.parent.namespace


def guess_association(el, parent):
    if el.find("type") is None or el.find("type").find("ref") is None:
        return None

    name = el.find("type").find("ref").text

    for cls in parent.classes:
        if cls.name == name:
            return cls
        elif cls.name in TYPEDEF_MAP and TYPEDEF_MAP[cls.name] == name:
            return cls
    warn("Association not found for name: "+name)
    return None


def parse_index(xml_path, root):
    els = ET.parse(path.join(xml_path, 'index.xml')).getroot()
    root.forward_declared_structs = []

    for el in els:
        if el.attrib['kind'] == 'struct':
            e = ET.parse(path.join(xml_path, "{}.xml".format(el.attrib['refid']))).getroot()[0]
            if 'bodyfile' not in e.find('location').attrib:
                root.forward_declared_structs.append(e)

    for el in els:
        if el.attrib['kind'] == 'namespace':
            name = el.attrib['refid']
            parse_namespace(xml_path, root,
                    ET.parse(path.join(xml_path, '{}.xml'.format(name))).getroot()[0])
        elif el.attrib['kind'] == 'dir':
            parse_dir(xml_path, root,
                    ET.parse(path.join(xml_path, "{}.xml".format(el.attrib['refid']))).getroot()[0])
            

def parse_dir(xml_path, parent, el):
    compoundname = el.find('compoundname').text
    name = compoundname.rsplit('/', 1)[-1]
    message(bcolors.BOLD, "Dir:", name)
    log("Dir: "+name)
    ns = E('ns', name, parent, el)


    for e in parent.forward_declared_structs:
        parent_dir = e.find('location').attrib['file'].rsplit('/', 1)[0]
        if parent_dir == compoundname:
            parse_class(xml_path, ns, e)

    for inner_el in el.findall("innerfile"):
        el = ET.parse(path.join(xml_path, "{}.xml".format(inner_el.attrib['refid']))).getroot()[0]
        parse_file(xml_path, ns, el)

def parse_namespace(xml_path, parent, el):
    name = el.find('compoundname').text.rsplit("::", 1)[-1]
    message(bcolors.BOLD, "Namespace:", name)
    log("NS: "+name)
    ns = E('ns', name, parent, el)

    parse_common(xml_path, ns, el)
    return ns

def parse_file(xml_path, parent, el):
    name = el.find('compoundname').text.rsplit("::", 1)[-1]
    message(bcolors.BOLD, "File:", name)
    log("F: "+name)
    #f = E('f', name, parent, el)
    parse_common(xml_path, parent, el)


def parse_class(xml_path, parent, el):
    name = el.find('compoundname').text.rsplit("::", 1)[-1]
    log("CLASS: "+name)
    if name[0] == '_':
        return
    cls = E('cls', name, parent, el)

    parse_common(xml_path, cls, el)
    

def parse_common(xml_path, parent, el_root):
    for el in el_root.findall("innerclass"):
        if el.attrib['prot'] == 'public':
            el = ET.parse(path.join(xml_path, "{}.xml".format(el.attrib['refid']))).getroot()[0]
            parse_class(xml_path, parent, el)

    for el in el_root.findall("innernamespace"):
        el = ET.parse(path.join(xml_path,"{}.xml".format(el.attrib['refid']))).getroot()[0]
        assert(el.attrib["kind"] == "namespace")
        parse_namespace(xml_path, parent, el)

    for el_sec in el_root.findall("sectiondef"):
        kind = el_sec.attrib["kind"]
        if "package" not in kind and "private" not in kind:
            for el in el_sec.findall("memberdef"):
                kind = el.attrib["kind"]
                if kind == "typedef":
                    parse_typedef(parent, el)
                elif kind == "function":
                    parse_function(parent, el)
                elif kind == "enum":
                    parse_enum(parent, el)
                elif kind == "variable":
                    parse_variable(parent, el)
                elif kind == "property":
                    parse_property(parent, el)
                else:
                    warn("No parsing for memberdef kind: {}".format(kind))


def parse_typedef(parent, el):
    name = el.find('name').text
    t = el.find('type')
    if t.find('ref') is not None:
        if t.text is not None:
            TYPEDEF_NAME_REVERSE[name] = t.text + " " + t.find('ref').text
        t = t.find('ref').text
    else:
        t = t.text
    if t is not None:
        log("typedef "+t+" "+name)
        TYPEDEF_MAP[t] = name

def parse_function(parent, el):
    name = el.find('name').text
    log("FN: "+name)
    E('fn', name, parent, el)

def parse_enum(parent, el):
    name = el.find('name').text
    if name[0] == '_':
        return
    log("ENUM: "+name)
    en = E('enum', name, parent, el)
    for el_ev in el.findall("enumvalue"):
        E('ev', el_ev.find("name").text, en, el_ev)

def parse_variable(parent, el):
    name = el.find('name').text
    if name[0] == '_':
        return
    log("VAR: "+name)
    E('def', name, parent, el)

def parse_property(parent, el):
    el.remove(el.find('initializer'))
    name = el.find('name').text
    if name[0] == '_':
        return
    log("VAR: "+name)
    E('def', name, parent, el)


def parse_type_to_html(el, ctx):
    t = el.find("type")
    if t is not None:
        res = (t.text or "").strip()
        if 'ignore_type_symbols' in ctx['settings']:
            for i in ctx['settings']['ignore_type_symbols']:
                res = res.replace(i, '').strip()
        ref = t.find("ref")
        if ref is not None:
            if 'refid' not in ref.attrib:
                error("expected refid")
                return ""
            if ref.attrib['refid'] not in BYREF:
                error("Tried to create reference to", ref.attrib['refid'], "but it doesn't exist in: ", BYREF.keys())
                return ""
            r = BYREF[ref.attrib['refid']]
            name = r.name
            if name in TYPEDEF_MAP:
                name = TYPEDEF_MAP[name]
            #if name in TYPEDEF_NAME_REVERSE:
                #name = TYPEDEF_NAME_REVERSE[name]
            res += ' <a class="{}" href="{}">{}</a>'.format(r.t, url_relativize(ctx, r.url), name)
            res = res.strip()
            if ref.tail is not None:
                res += ref.tail.strip()
        else:
            # Doxygen doesn't give references for anything other than classes.
            res = annotate_type(ctx, res)
        if t.tail is not None:
            res += t.tail.strip()
        return res
    return ""

def get_class(j, name):
    for cls in j["classes"]:
        if cls["name"] == name:
            c = copy(cls)
            c["name"] = c["name"].split("::")[-1]
            if 'inner' in c:
                l = []
                for i in c['inner']:
                    l.append(get_class(j, i['name']))
                c['inner'] = l
            return c

def find_name_in_E(ns, name):
    matches = []

    if not ns:
        warn("Failed to find name in null")
        return matches

    for c in ns.classes:
        if c.name.casefold() == name.casefold():
            matches.append(c)

    for c in ns.enums:
        if c.name.casefold() == name.casefold():
            matches.append(c)

    for c in ns.enum_values:
        if c.name.casefold() == name.casefold():
            matches.append(c)

    for c in ns.functions:
        if c.name.casefold() == name.casefold():
            matches.append(c)

    for c in ns.namespaces:
        if c.name.casefold() == name.casefold():
            matches.append(c)

    return matches


@contextfilter
def url_relativize(ctx, url):
    page = ctx["page"]
    url = page.settings['docs_url'].rstrip('/') + url
    if url == page.url:
        return ""
    f = [i for i in page.url.split("/") if i]
    t = [i for i in url.split("/") if i]
    while len(f) and len(t):
        if f[0] == t[0]:
            del f[0]
            del t[0]
        else:
            break
    if not t:
        return "../"*len(f)
    else:
        r = "{}{}".format("../"*len(f), "/".join(t))
        if t[-1][0] != '#':
            r += "/"
        return r

def find_path(r, ps, guess_simmilar):
    for p in ps:
        m = find_name_in_E(r, p)
        if guess_simmilar:
            m.extend(find_name_in_E(r, guess_name(r.namespace, p)))
        if len(set(m)) == 1:
            r = m[0]
        elif m:
            if len(set((i.name, i.t) for i in m)) > 1:
                warn("ambiguous type path: "+str(ps))
                for i in m:
                    print("  {} ({}) {}".format(i.name, i.t, i.lang.id))
            r = m[0]
        else:
            return []
    return [r]

def guess_name(ns, t):
    # Guess if this language has a different naming convention:

    # Convert from cammelCasing to underscores
    g = ""
    for l in t:
        if g and l.isupper():
            g += "_"+l.lower()
        else:
            g += l.lower()
    yield g

    # c style with prefixing namespace
    yield ns.name+"_"+g


def normalize_name(ns, name):
    normalized_name = name.replace("::", ".")
    if normalized_name.startswith(ns.name+"_"):
        normalized_name = normalized_name[len(ns.name)+1:]
    return normalized_name


def annotate_type(ctx, name, content=None, expect_match=False, guess_simmilar=False):
    if type(name) is not str or not name or name[0].isdigit():
        return name

    readonly = False
    if name.startswith("readonly "):
        readonly = True
        name = name[9:]

    arr = ""
    if name.endswith(" []"):
        arr = "[]"
    elif name.endswith(" *"):
        arr = "*"
    elif name.endswith(" &&"):
        arr = "&&"
    elif name.endswith(" &"):
        arr = "&"
    if arr:
        name = name[:-(len(arr)+1)]

    ns = ctx['this'].namespace
    lang = ctx['this'].lang
    normalized_name = normalize_name(ns, name)
    if normalized_name.endswith("()"):
        normalized_name = normalized_name[:-2]
        expects_function = True
    else:
        expects_function = False
    matches = find_name_in_E(ns, normalized_name)
    matches.extend(find_name_in_E(ctx['this'], normalized_name))
    if guess_simmilar:
        for g in guess_name(ns, normalized_name):
            matches.extend(find_name_in_E(ns, g))
            matches.extend(find_name_in_E(ctx['this'], g))

    if "." in normalized_name:
        ps = normalized_name.split(".")
        matches.extend(find_path(ns, ps, guess_simmilar))
        matches.extend(find_path(ns.root, ps, guess_simmilar))

    for c in ctx['functions']:
        if c.name == normalized_name:
            matches.append(c)


    if len(set(matches)) > 1:
        warn("Found multiple type matches for name '{}':".format(name))
        for c in matches:
            print("   ", c.sig, "({})".format(c.t), c.lang.id)

    cls = ""
    if matches:
        cls = matches[0].t
        if expects_function and cls != "fn":
            warn("Type annotator expected a function, got {}: {}".format(cls, name))
        r = '<a class="{}" href="{}">{}</a>'.format(cls, url_relativize(ctx, matches[0].url), content or name)
        # TODO If doxygen didn't generate a ref, we shouldn't have found a class?
    else:
        if expect_match:
            warn("No matches found for type annotation '"+name+"' in language: "+lang.id)
        r = name
    if arr:
        r += arr
    if readonly:
        r = "readonly "+r
    return r

def map_macro(d, macro):
    r = []
    for i in d:
        r.append(macro(i))
    return r

def img(value, alt=""):
    t = Image.open("theme/img/"+value+".png")
    w, h = t.size
    extra = ""
    if alt:
        extra += ' alt="{}"'.format(alt)
    return '<img src="/img/{}.png" width="{}" height="{}"{}>'.format(value, w, h, extra)



def format_doc(ctx, el, identifier, breakless, nolink):
    if el.tag in ("simplesect", "parameterlist"):
        ignore("Skipping {} docs.".format(el.tag))
        return ""
    text = el.text
    tpl = " {}{}"
    if el.tag == "para":
        if not breakless:
            tpl = "<p>{}</p>{}"
    elif el.tag in ("programlisting", "verbatim"):
        tpl = "<pre>{}</pre>{}"
    elif el.tag == "codeline":
        tpl = "{}{}\n"
    elif el.tag == "highlight":
        tpl = "{}{}"
    elif el.tag == "ulink":
        if not nolink:
            url = el.get("url")
            found = None
            if url.startswith("struct.") and url.endswith(".html"):
                tpl = annotate_type(ctx, url[7:-5], "{}")+"{}"
            elif not url.startswith("http://") and not url.startswith("https://"):
                warn("Probably a broken link: "+url)
                tpl = " <a href=\"{}\">{{}}</a>{{}}".format(url)
            else:
                tpl = " <a href=\"{}\">{{}}</a>{{}}".format(url)
    elif el.tag == "ref":
        if not nolink:
            e = BYREF[el.get("refid")]
            tpl = " <a class=\"{}\" href=\"{}\">{{}}</a>{{}}".format(e.t, url_relativize(ctx, e.url))
    elif el.tag == "computeroutput":
        if not nolink and text and ('.' in text or text.endswith("()")):
            # We're guessing that this might be a link to a type
            tpl = ' <code>'+annotate_type(ctx, text, expect_match=True)+'</code>{}{}'
            text = ''
        else:
            tpl = " <code>{}</code>{}"
    elif el.tag == "itemizedlist":
        tpl = "<ul>{}</ul>{}"
    elif el.tag == "listitem":
        tpl = "<li>{}</li>{}"
    elif el.tag == 'bold' and text == 'Examples':
        # doxypypy converts example headers to be use the @b command. To make
        # python consistant with other language docs we convert this one case
        # to be a header.
        tpl = " <h4>{}</h4>{}"
    elif el.tag in ("emphasis", 'bold'):
        tpl = " <em>{}</em>{}"
    elif el.tag == "heading":
        l = int(el.get("level")) + 3
        tpl = "<h{}>{{}}</h{}>{{}}".format(l,l)
    elif el.tag == "title":
        tpl = "<h4>{}</h4>{}"
    elif el.tag in ("sp", "sect1", "linebreak"):
        pass
    else:
        warn("unhandled doc tag: "+el.tag)

    r = ""
    if el.tag == "programlisting":
        assert(not text)
        for i in el:
            r += format_doc(ctx, i, identifier, breakless, nolink)
        try:
            lexer = get_lexer_by_name(ctx["lang"].id)
        except ValueError:
            try:
                lexer = guess_lexer(r)
            except ValueError:
                lexer = get_lexer_by_name('text')
        # TODO Add to a list for tests
        formatter = CodeHtmlFormatter()
        r = highlight(r, lexer, formatter)
    else:
        r = (text or "").strip()
        for i in el:
            r += format_doc(ctx, i, identifier, breakless, nolink)

    if el.tag == "computeroutput":
        r = r.strip(' ')
    return tpl.format(r, (el.tail or "").rstrip())

style_map = {'code': 'code', 'italic': 'i', 'bold': 'b'}
@contextfilter
def docstr(ctx, value, identifier, breakless=False, nolink=False):
    if value:
        r = ""
        for i in value:
            r += format_doc(ctx, i, identifier, breakless, nolink)
        return r
    return ""



class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

warn_count = 0
def warn(msg):
    global warn_count
    warn_count += 1
    print(bcolors.WARNING+"WARNING:"+bcolors.ENDC, msg)
error_count = 0
def error(*msg):
    global error_count
    error_count += 1
    print(bcolors.FAIL+"ERROR:"+bcolors.ENDC, *msg)

ignore_count = 0
def ignore(msg):
    global ignore_count
    ignore_count += 1
    #print(bcolors.BOLD+"IGNORE:"+bcolors.ENDC, msg)

CodeHtmlFormatter.warn = warn
ThumbExtension.error = error

def log(msg):
    pass
    #print(msg)

def message(color, *msg):
    print(color+' '.join(msg)+bcolors.ENDC)

S = document.getElementById('search');
E = S.children[0];
M = S.children[1];
C = document.getElementById('c');
E.oninput = function(){
    var newM = document.createElement("ul");
    newM.className = "search";
    if (E.value) {
        C.className = 'hidden';
        var search = E.value.toLowerCase();
        function match(d, m) {
            var name = d[0];
            var url = d[1];
            var desc = d[2];
            var found = d[m].toLowerCase().indexOf(search);
            if (found >= 0) {
                var e = document.createElement("li");
                var a = document.createElement("a");
                var n = document.createElement("div");
                if (m == 0) {
                    var f = name.substr(found, search.length);
                    n.innerHTML = name.slice(0, found)+"<span class=match>"+f+"</span>"+name.slice(found+search.length);
                } else {
                    n.appendChild(document.createTextNode(name));
                }
                a.href = document.location.pathname.split("/").splice(0,3).join("/") + "/" + url;
                n.className = INDEX[t][0];
                e.appendChild(a);
                var p = document.createElement("p");
                if (m == 2) {
                    var f = desc.substr(found, search.length);
                    desc = desc.slice(0, found)+"<span class=match>"+f+"</span>"+desc.slice(found+search.length);
                }
                p.innerHTML = desc;
                a.appendChild(n);
                a.appendChild(p);
                newM.appendChild(e);
            }
        }
        for (var t in INDEX) {
            var arr = INDEX[t][1];
            for (var i in arr) {
                match(arr[i], 0);
            }
        }
        for (var t in INDEX) {
            var arr = INDEX[t][1];
            for (var i in arr) {
                match(arr[i], 2);
            }
        }
    } else {
        C.className = '';
    }
    S.replaceChild(newM, M);
    M = newM;
};
window.onload = function() {
    E.onpropertychange = E.oninput; // for IE8
    document.addEventListener('keydown', function(ev) {
        if (document.activeElement.tagName === "INPUT") return;
        if (ev.ctrlKey || ev.altKey || ev.metaKey) return;

        if (ev.key == "s") {
            ev.preventDefault();
            E.focus();
        }
    }, false);
    window.onhashchange = function() {
        E.value = "";
        E.oninput();
        location.hash = location.hash;
    }
}

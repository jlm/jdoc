from markdown import Extension
from markdown.preprocessors import Preprocessor
from markdown.inlinepatterns import Pattern
from markdown.util import etree, AtomicString
import re

ABBR_REF_RE = re.compile(r'[%]\[(?P<span>[^\]]*)\][ ]?:\s*(?P<class>.*)')

class HighlightExtension(Extension):
    """ Highlight Extension for Python-Markdown. """

    def extendMarkdown(self, md, md_globals):
        """ Insert AbbrPreprocessor before ReferencePreprocessor. """
        md.preprocessors.add('highlight', AbbrPreprocessor(md), '_begin')
        
           
class AbbrPreprocessor(Preprocessor):
    """ Abbreviation Preprocessor - parse text for span references. """

    def run(self, lines):
        '''
        Find and remove all Abbreviation references from the text.
        Each reference is set as a new AbbrPattern in the markdown instance.
        
        '''
        new_text = []
        for line in lines:
            m = ABBR_REF_RE.match(line)
            if m:
                span = m.group('span').strip()
                cls = m.group('class').strip()
                self.markdown.inlinePatterns['span-%s'%span] = \
                    AbbrPattern(self._generate_pattern(span), cls)
            else:
                new_text.append(line)
        return new_text
    
    def _generate_pattern(self, text):
        '''
        Given a string, returns an regex pattern to match that string. 
        
        'HTML' -> r'(?P<span>[H][T][M][L])' 
        
        Note: we force each char as a literal match (in brackets) as we don't 
        know what they will be beforehand.

        '''
        chars = list(text)
        for i in range(len(chars)):
            chars[i] = r'[%s]' % chars[i]
        return r'(?P<span>\b%s\b)' % (r''.join(chars))


class AbbrPattern(Pattern):
    """ Abbreviation inline pattern. """

    def __init__(self, pattern, cls):
        super(AbbrPattern, self).__init__(pattern)
        self.cls = cls

    def handleMatch(self, m):
        span = etree.Element('span')
        span.text = AtomicString(m.group('span'))
        span.set('class', self.cls)
        return span

def makeExtension(*args, **kwargs):
    return HighlightExtension(*args, **kwargs)

import click

@click.group()
def main():
    pass

@main.command(help="Generate documentation")
def gen():
    from jdoc.generator import Generator
    import json
    try:
        settings = json.load(open("settings.json"))
    except Exception as e:
        config_error(e)
    g = Generator(settings)
    g.generate_www()

class Language:
    def __init__(self, id, name, namespace_name="Namespace", namespace_sep="::"):
        self.id = id
        self.name = name
        self.namespace_name = namespace_name
        self.namespace_sep = namespace_sep

supported_languages = [
    Language("python", "Python", "Module", "."),
    Language("cpp", "C++"),
    Language("c", "C", "Module"),
    Language("csharp", "C#"),
]


@main.command(help="Create a new jdoc project")
@click.argument("name")
def new(name):
    from collections import OrderedDict
    import os
    import json
    import shutil

    try:
        os.mkdir(name)
        shutil.copytree(os.path.join(os.path.dirname(__file__), 'theme'), os.path.join(name, 'theme'))
        shutil.copytree(os.path.join(os.path.dirname(__file__), 'site'), os.path.join(name, 'site'))
    except Exception as e:
        config_error(e)

    settings = OrderedDict([
        ('name', 'My Project'),
        ('site_url', '/'),
        ('docs_url', '/docs/'),
        ('domain', 'http://localhost:8000'),
        ('docs', OrderedDict([(lang.id, OrderedDict([('name', lang.name), ('input', ""), ('root', "")])) for lang in supported_languages])),
        ('build_dir', 'build'),
        ('output_dir', 'www'),
        ('theme_dir', 'theme'),
        ('site_dir', 'site'),
        ('copyright', ''),
        ])
    with open(os.path.join(name, "settings.json"), 'w') as fp:
        fp.write(json.dumps(dict(settings.items()), indent='    '))


def config_error(*msg):
    import sys
    print("ERROR:", *msg)
    print("Aborting")
    sys.exit(-1)

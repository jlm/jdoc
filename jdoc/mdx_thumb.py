from markdown import Extension
from markdown.blockprocessors import BlockProcessor
from markdown.util import etree
import re
from PIL import Image
import os
import shutil


class ThumbProcessor(BlockProcessor):
    def __init__(self, ex, *args, **kwargs):
        self.ex = ex
        super(ThumbProcessor, self).__init__(*args, **kwargs)
    RE = re.compile(r'^\!(?P<no>no)?thumb\[(?P<alt>[a-zA-Z0-9\- ]*)\]\((?P<url>.+)\)(?P<id>#[a-zA-Z0-9\-]+)?')
    def test(self, parent, block):
        return bool(self.RE.search(block))
    def run(self, parent, blocks):

        block = blocks.pop(0)
        m = self.RE.search(block)

        s = self.ex.getConfig("source_path")
        e = self.ex.getConfig("export_path")
        root_url = self.ex.getConfig("url")

        os.makedirs(os.path.join(e, "img/thumb"), mode=0o777, exist_ok=True)

        size = 880
        url_name, *ops = m.group('url').split(' ')
        ops = dict([o.split(':') for o in ops])
        p = "img/{}.png".format(url_name)
        if 'size' in ops:
            size = int(ops['size'])
        source = os.path.join(s, p)
        thumb = os.path.join(e, "img/thumb/{}_{}.png".format(url_name, size))
        thumb_url = os.path.join(root_url, "img/thumb/{}_{}.png".format(url_name, size))
        url = os.path.join(root_url, p)
        t = Image.open(source)
        if not m.group('no') and (t.size[0] > size or t.size[1] > size):
            if not os.path.isfile(thumb):
                t.thumbnail((size, size), Image.LANCZOS)
                t.save(thumb)
            a = etree.SubElement(parent, 'a')
            a.set("href", url)
            im = etree.SubElement(a, 'img')
            im.set("src", thumb_url)
            im.set("alt", m.group('alt'))
            im.set("class", 'thumb')

            # Size values aren't updated on original image, so we have to re-open it.
            t = Image.open(thumb)
            im.set("width", str(t.width))
            im.set("height", str(t.height))
        else:
            im = etree.SubElement(parent, 'img')
            im.set("src", url)
            im.set("alt", m.group('alt'))
            im.set("width", str(t.width))
            im.set("height", str(t.height))

        if m.group('id'):
            im.set('id', m.group('id')[1:])


class ThumbExtension(Extension):
    def __init__(self, **kwargs):
        self.config = {'export_path' :["", ""],
                       'source_path' : ["", ""],
                       'url' : ["", ""],
                       }
        super(ThumbExtension, self).__init__(**kwargs)
    def extendMarkdown(self, md, md_globals):
        md.parser.blockprocessors.add('thumb',
                ThumbProcessor(self, md.parser),
                '>empty')


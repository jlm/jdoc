from jdoc import config_error, supported_languages
from PIL import Image
from jinja2 import Template, Environment, FileSystemLoader, contextfilter, contextfunction
from jsmin import jsmin
import htmlmin
import os
from os import path
import sys
import time
import stat
import lesscpy
from subprocess import Popen
import re
import shutil
import json
import glob
import markdown
from jdoc.mdx_thumb import ThumbExtension
from jdoc.mdx_highlight import HighlightExtension
from jdoc.mdx_tabbedcode import CodeHtmlFormatter, TabbedCodeExt
from jdoc.mdx_apiref import APIRefExtension

from jdoc.parser import E, ET, parse_index, parse_namespace, format_doc, annotate_type, url_relativize

class Generator:
    def __init__(self, settings):
        self.settings = settings
        self.errors = []
        self.warnings = []

        if not any([ops['input'] for ops in settings['docs'].values()]):
            config_error("No input sources specified. Edit your settings.json file.")

        self.env = Environment(loader=FileSystemLoader(self.settings['theme_dir']),
                trim_blocks=True, lstrip_blocks=True,
                line_statement_prefix='#')
        self.env.filters['img'] = img
        self.env.filters['docstr'] = docstr
        self.env.filters['map_macro'] = map_macro
        self.env.filters['relativize'] = url_relativize
        self.t_site_base = self.env.get_template("site.html")
        self.t_doc_base = self.env.get_template("apidoc_base.html")
        self.t_namespace = self.env.get_template("namespace.html")
        self.t_root = self.env.get_template("root.html")
        self.t_class = self.env.get_template("class.html")
        self.t_enum = self.env.get_template("enum.html")


    def generate_www(self):
        self.sitemap_urls = set()

        bdir = self.settings['build_dir']
        odir = self.settings['output_dir']
        tdir = self.settings['theme_dir']
        site_dir = self.settings['site_dir']
        site_url = self.settings.get('site_url', '').strip('/')
        docs_url = self.settings.get('docs_url', '').strip('/')

        if not odir:
            config_error("Invalid output_dir configuration")

        os.makedirs(bdir, exist_ok=True)
        os.makedirs(path.join(odir, site_url), exist_ok=True)
        os.makedirs(path.join(odir, docs_url), exist_ok=True)

        ctx = {
            "settings": self.settings,
            "style": StyleDict(),
            "js": {},
            "page": None,
            "langs": [],
            'current_year': str(format(time.gmtime().tm_year)),
        }
        if site_url:
            ctx['site_url'] = '/'+site_url
        else:
            ctx['site_url'] = ''

        if docs_url:
            ctx['docs_url'] = '/'+docs_url
        else:
            ctx['docs_url'] = ''

        # Copy assets
        if path.exists(path.join(odir, site_url, "img")):
            shutil.rmtree(path.join(odir, site_url, "img"))
        if path.exists(path.join(tdir, "img")):
            shutil.copytree(path.join(tdir, "img"), path.join(odir, site_url, "img"))
        for f in os.listdir(path.join(tdir, "js")):
            if f.endswith(".js"):
                with open(path.join(tdir, 'js', f)) as fp:
                    m = jsmin(fp.read())
                    ctx['js'][f.split(".")[0]] = m
                    open(path.join(odir, site_url, f), 'w').write(m)

        # Process styles
        t = 0
        tsp = path.join(bdir, "style_ts")
        files = []
        dirty = False
        for f in os.listdir(path.join(tdir, 'style')):
            if f.endswith(".less"):
                files.append(f)
                t = max(path.getmtime(path.join(tdir, 'style', f)), t)
                if not dirty and not path.exists(path.join(bdir, "style_"+f)):
                    dirty = True
        if dirty or not path.exists(tsp) or open(tsp).read() != str(t):
            open(tsp, "w").write(str(t))
            dirty = True
            log(bcolors.HEADER, "Generating css...")

        for f in files:
            cached = path.join(bdir, "style_"+f)
            style_id = f.split(".")[0]
            if dirty:
                s = lesscpy.compile(open(path.join(tdir, 'style', f)), minify=True, xminify=True)
                log(bcolors.OKGREEN, "style:", style_id)
                ctx["style"][style_id] = s
                open(cached, "w").write(s)
            else:
                ctx["style"][style_id] = open(cached).read()

        # Generate doc definitions
        doxy = Template(open(path.join(path.dirname(__file__), "Doxyfile")).read())
        re_name = re.compile(r" .(?P<name>\w+) =>")
        re_quote = re.compile(r"([^\\])'")
        re_single_quote = re.compile(r"\\'")
        re_pre = re.compile(r"([^\[\]\{\},\"])$")

        py_filter = path.abspath(path.join(bdir, "py_filter"))
        with open(py_filter, "w") as fp:
            if os.name == 'nt':
                fp.write(sys.executable+" -m doxypypy.doxypypy -a -c %1\n")
            else:
                fp.write("#!/bin/bash\n"+sys.executable+" -m doxypypy.doxypypy -a -c $1\n")
                st = os.stat(path.join(bdir, "py_filter"))
                os.chmod(path.join(bdir, "py_filter"), st.st_mode | stat.S_IEXEC)

        lang_namespaces = {}
        for lang, doc in self.settings['docs'].items():
            inputs = ' '.join([' '.join(glob.glob(i)) for i in doc['input'].split(' ')])
            if not inputs:
                continue
            log(bcolors.HEADER, "Building", lang, "docs...")
            os.makedirs(path.join(odir, docs_url, lang), exist_ok=True)
            xml_path = path.join(bdir, "xml-"+lang)
            if path.exists(xml_path):
                shutil.rmtree(xml_path)
            f = "doxygen_"+lang+".conf"
            with open(path.join(bdir, f), "w") as fp:
                fp.write(doxy.render({"input": " ".join([path.join("..", i) for i in inputs.split(' ')]),
                    'lang':lang,
                    'py_filter':py_filter}))
            null = open(os.devnull, 'wb')
            Popen(["doxygen", f], cwd=bdir, stderr=null, stdout=null).wait()


            # Parse XML into our data format
            root = E('root', '', None, None)
            root.lang = [i for i in supported_languages if i.id == lang][0]
            ctx['langs'].append(root)

            namespaces = None
            if 'root' in doc:
                namespaces = doc['root']
                if type(namespaces) is str and len(namespaces):
                    namespaces = [namespaces]
            if namespaces:
                for f in os.listdir(xml_path):
                    if f.startswith("namespace"):
                        n = f[9:-4]
                        if n in namespaces:
                            lang_namespaces[lang] = parse_namespace(xml_path,
                                    root, ET.parse(path.join(xml_path, f)).getroot()[0])
            else:
                lang_namespaces[lang] = None
                parse_index(xml_path, root)
                lang_namespaces[lang] = root.namespace

            def build(e, tpl):
                ctx['page'] = Page("/"+docs_url+e.url, self)
                out_dir = path.join(odir, docs_url, e.url.strip('/'))
                os.makedirs(out_dir, exist_ok=True)
                with open(path.join(out_dir, "index.html"), "w") as fp:
                    fp.write(htmlmin.minify(tpl.render(**e.__dict__, **ctx),
                        remove_comments=True,
                        reduce_boolean_attributes=True,
                        remove_empty_space=True,
                        keep_pre=True))

                for ns in e.namespaces:
                    build(ns, self.t_namespace)
                for f in e.files:
                    build(f, self.t_namespace)
                for c in e.classes:
                    build(c, self.t_class)
                for c in e.enums:
                    build(c, self.t_enum)
            build(root, self.t_root)
            root.build_json()
            with open(path.join(odir, docs_url, lang, 'index.js'), 'w') as fp:
                fp.write(jsmin("INDEX = {};".format(json.dumps(tuple(root.json.items())))))


        # Generate site
        pages = []
        page_filenames = []
        for f in os.listdir(site_dir):
            if f.endswith('.md'):
                page_filenames.append(f)
                n = f.rsplit(".", 1)[0]
                url = ctx['site_url']
                name = n.replace("_", " ").replace("-", " ")
                if f != 'index.md':
                    url += n+"/"
                else:
                    name = ""
                page = Page(url, self)
                pages.append({
                    "name": name,
                    "filename": f,
                    "url": url,
                    "title": page.title,
                    "canonical_url": page.canonical_url,
                })
        ctx["pages"] = pages
        ex_thumb = ThumbExtension(source_path=site_dir,
                export_path=os.path.join(odir, ctx['site_url']),
                url=ctx['site_url'])
        ex_highlight = HighlightExtension()
        ex_tabbedcode = TabbedCodeExt()
        ex_apiref = APIRefExtension()
        current_url = ""
        def annotate(t):
            for root in ctx['langs']:
                ns = lang_namespaces[root.lang.id]
                if ns:
                    c = {"this":ns, "page":Page(current_url, self)}
                    c.update(ns.__dict__)
                    lang = self.settings['docs'][root.lang.id]['name']
                    r = annotate_type(c, t, content=lang, expect_match=True, guess_simmilar=True)
                    if r.startswith('<a '):
                        yield r
        APIRefExtension.annotate = annotate
        APIRefExtension.warn = lambda *args: log(bcolors.WARNING, *args)
        for p in pages:
            with open(os.path.join(site_dir, p["filename"])) as md_in:
                out_dir = path.join(odir, p['url'])
                log(bcolors.OKGREEN, "page:", out_dir)
                current_url = p['url']
                TabbedCodeExt.found_highlight = False
                TabbedCodeExt.found_tabbed = False
                md = markdown.Markdown(extensions=[ex_thumb, ex_highlight, ex_tabbedcode, ex_apiref,
                            'markdown.extensions.footnotes',
                            'markdown.extensions.def_list',
                            'markdown.extensions.toc',
                            'markdown.extensions.admonition',
                            'markdown.extensions.meta',
                            'markdown.extensions.abbr',
                            ])
                ctx['content'] = md.convert(Template(md_in.read()).render(**ctx))
                ctx['page'] = p
                os.makedirs(out_dir, exist_ok=True)
                tpl = self.t_site_base
                if 'template' in md.Meta:
                    tpl = self.env.get_template(md.Meta['template'][0])
                if 'title' in md.Meta:
                    p['title'] = md.Meta['title'][0]
                ctx['meta'] = md.Meta
                with open(out_dir+"index.html", "w") as fp:
                    fp.write(htmlmin.minify(tpl.render(**ctx,
                            has_code=TabbedCodeExt.found_highlight,
                            has_tabbed=TabbedCodeExt.found_tabbed,
                            has_admointion="class=\"admonition" in ctx['content']),
                        remove_comments=True,
                        reduce_boolean_attributes=True,
                        remove_empty_space=True,
                        keep_pre=True))

class Page:
    def __init__(self, url, generator):
        self.url = url
        self.title = url.strip("/").split("/")[-1].replace("-", " ").replace("_", " ").title()
        self.canonical_url = generator.settings['domain']+url
        generator.sitemap_urls.add(url)
        self.settings = generator.settings




def map_macro(d, macro):
    r = []
    for i in d:
        r.append(macro(i))
    return r

@contextfilter
def img(ctx, value, alt=""):
    t = Image.open(path.join(ctx['settings']['theme_dir'], "img",value+".png"))
    w, h = t.size
    extra = ""
    if alt:
        extra += ' alt="{}"'.format(alt)
    return '<img src="/img/{}.png" width="{}" height="{}"{}>'.format(value, w, h, extra)

@contextfilter
def docstr(ctx, value, identifier, breakless=False, nolink=False):
    if value:
        r = ""
        for i in value:
            r += format_doc(ctx, i, identifier, breakless, nolink)
        return r
    return ""

class StyleDict:
    """
    We use this instead of a dictionary so that jinja2 can access styles.clear
    and not get the dict's clear method.
    """
    def __init__(self):
        self.__d = {}
    def __getitem__(self, name):
        return self.__d[name]
    def __setitem__(self, name, value):
        self.__d[name] = value

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def log(color, *msg):
    print(color+' '.join(msg)+bcolors.ENDC)

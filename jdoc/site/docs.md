# {{ settings.name }} Documentation

{% for l in langs %}
* [{{ l.lang.name }} Documentation]({{ docs_url }}/{{ l.lang.id }}/)
{% endfor %}

from markdown import Extension
from markdown.preprocessors import Preprocessor, util
import re

RE = re.compile(r'\`\`[\w\.\(\)]+\`\`')

class APIRefExtension(Extension):
    def extendMarkdown(self, md, md_globals):
        md.preprocessors.add('apiref', APIRefProcessor(md), '_begin')

class APIRefProcessor(Preprocessor):
    def run(self, root):
        out = []
        def match(m):
            s = m.group(0)[2:-2]
            an = list(APIRefExtension.annotate(s))
            if len(an) == 0:
                APIRefExtension.warn("No annotations found for API reference "+s)
                return "<code>{}</code>".format(s)
            return "<code>{} ({})</code>".format(s, ", ".join(an))
        for e in root:
            out.append(RE.sub(match, e))
        return out

def makeExtension(*args, **kwargs):
    return APIRefExtension(*args, **kwargs)

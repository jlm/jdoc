#!/usr/bin/env python

from setuptools import setup

setup(
    name='jdoc',
    version='1.0',
    description='Documentation site generator',
    author='Joseph Marshall',
    author_email='jlmrshl@gmail.com',
    url='https://bitbucket.org/jlm/jdoc/',
    packages=['jdoc'],
    scripts=['bin/jdoc'],
    install_requires=['click', 'jinja2', 'pygments', 'htmlmin', 'jsmin', 'lesscpy', 'Pillow', 'doxypypy', 'markdown'],
    include_package_data=True,
)
